//block cam

import processing.video.*;
Capture cam;
ArrayList<Block> blocks = new ArrayList<Block>();

PGraphics canvas;
PFont emojiFont;
int block = 720 / 48;

void setup () {
  size(720, 720, P3D);
  noStroke();
  canvas = createGraphics(48, 48, P2D);
  camStart();
}

void draw () {
   if (cam.available() == true) {
    cam.read();
    cam.resize(0, 48);
    blocks();
   }
}

void blocks () {
  canvas.beginDraw();
  canvas.imageMode(CENTER);
  canvas.image(cam, 24, 24);
  canvas.endDraw();
  canvas.loadPixels();

  for (int y = 0; y < 48; y++) {
    for (int x = 0; x < 48; x++) {
      blocks.add(new Block(x, y, canvas.pixels[(y * canvas.width) + x]));
    }
  }
  for (int i = 0; i < blocks.size(); i++) {
    Block b = blocks.get(i);
    if (b.alive()) {
      b.display();
    }
  }
  blocks.clear();
}
class Block {
  color c;
  float weight;
  int x;
  int y;
  int r;
  int g;
  int b;
  int a;
  int fade = 255;
  
  int xPos = 0;
  int yPos = 0;
  float zPos = 0;
  
  Block (int x, int y, int argb) {
    c = pixelColor(argb);
    weight = brightness(c);
    this.x = x;
    this.y = y;
  }
  
  //https://py.processing.org/reference/rightshift.html
  private color pixelColor (int argb) {
    a = (argb >> 24) & 0xFF;
    r = (argb >> 16) & 0xFF;
    g = (argb >> 8) & 0xFF;
    b = argb & 0xFF;
    return color(r, g, b, a);
  }
  public void display () {
    
    fill(color(r, g, b, 127));
    translate(x*block, y*block, zPos);
    box( block, block, weight);
    translate(-x*block, -y*block, -zPos);
  }
  public boolean alive () {
    return fade > 0;
  }
}


//https://processing.org/reference/libraries/video/Capture.html
void camStart () {
  String[] cameras = Capture.list();
  if (cameras.length == 0) {
    exit();
  } else {
    cam = new Capture(this, cameras[0]);
    cam.start();     
  } 
}
