import java.util.Collections;

float 
windowSize = 1,
x, y, z, 
lastX = 360, 
lastY = 360, 
lastZ = 0, 
s = 0,
points;

int renderX, renderY;
ArrayList <Point> randomMap = new ArrayList<Point>();
Point p;
Point p2;

//https://groups.csail.mit.edu/graphics/classes/6.837/F03/models/teddy.obj
//no licence found!
String shapePath = "teddy.obj";
PShape shape;
PGraphics render;

void setup () {
  size(720,720,P3D);
  rectMode(CENTER);
  noFill();
  frameRate(12);
  colorMode(HSB,255);
  strokeWeight(2);
  
  shape = loadShape(shapePath);
  shape.scale(12);
  shape.rotateX(-PI);
  shape.rotateY(PI * .5);
  shape.setFill(color(255));
  render = createGraphics(720, 720, P3D);
}

void draw () {
  clear();
  drawRender();
  s = sin(frameCount) * 25;
  if (frameCount % 2 == 0) {
    background(color(random(255), s, 125));
    stroke(245); 
  } else {
    background(color(random(255), s, 125));
    stroke(10);
  }
  randomMap.clear();
  for (int i = 0; i < render.width * render.height; i++) {
    if (render.pixels[i] > 0) {
      randomMap.add( new Point(i, render.pixels[i] > 0) );
    }
  }
  
  Collections.shuffle(randomMap);
  points = randomMap.size();
  for (int i = 0; i < points; i++) {
    p = randomMap.get(i);
    p2 = randomMap.get(floor(random(points)));
    renderX = p.i % render.width;
    renderY = floor(p.i / render.width);
    
    x = renderX;
    y = renderY; 
    z = noise(i);//random(windowZ - windowSize, windowZ + windowSize);
    
    modifiedLine(lastX, lastY, x, y);
    
    lastX = x;
    lastY = y;
    lastZ = z;
  }
}

void drawRender () {
  render.beginDraw();
  render.clear();
  //render.background(0);
  render.fill(255);
  render.shape(shape, 360, 360);
  render.endDraw();
  render.loadPixels();
  shape.rotateY(0.05);
}

void modifiedLine (float x1, float y1, float x2, float y2) {
  //float len = dist(x1, y1, 360, 360) + dist(x2, y2, 360, 360);
  //line(x1, y1, 360, 360);
  //line(360, 360, x2, y2);
  float mx1 = x1 + random(x1 - 20, x1 + 20); 
  float my1 = y1 + random(y1 - 20, y1 + 20); 
  float mx2 = x2 + random(x2 - 20, x2 + 20);
  float my2 = y2 + random(y2 - 20, y2 + 20);
  
  curve( mx1, my1, x1, y1, x2, y2, mx2, my2 );
}

class Point{
  int i;
  boolean on;
  Point (int i, boolean on) {
    this.i = i;
    this.on = on;
  }
}
