int renderX;
int renderY;
int time = 0;

// https://people.sc.fsu.edu/~jburkardt/data/obj/magnolia.obj
// LICENSE
// https://people.sc.fsu.edu/~jburkardt/txt/gnu_lgpl.txt
String shapePath = "magnolia.obj";
PShape s;

PGraphics render;

ArrayList<Particle> particles = new ArrayList<Particle>();
Particle p;

void setup () {
  size(512, 512, P3D);
  frameRate(24);
  s = loadShape(shapePath);
  s.scale(2.5);
  s.rotateX(-PI * .75);
  s.rotateY(PI * .5);
  render = createGraphics(512, 512, P3D);
  colorMode(HSB, 255);
}

void draw () {
  clear();
  background(color(200, 50, 30));
  drawRender();
  drawParticles();
}

void drawRender () {
  render.beginDraw();
  render.clear();
  render.shape(s, 256, 210);
  render.endDraw();
  render.loadPixels();
  s.rotateY(0.01);
}

void drawParticles () {
  for (int i = 0; i < render.width * render.height; i++) {
    if (render.pixels[i] > 0) {
      renderX = i % render.width;
      renderY = floor(i / render.width);
      particles.add( new Particle(renderX, renderY, 0));
    }
  }
  
    for (int i = 0; i < particles.size(); i++) {
     p = particles.get(i);
     if (p.done()) {
        particles.remove(i);
     } else {
       p.run();
     }
    }
}

class Particle {
  PVector position;
  PVector velocity;
  PVector acceleration;
  
  int age = 0;
  float h = random(80) + 180;
  float b = 255;
  color fill = color(h, 255, 255);
  boolean up = random(2) > 1;
  
  boolean active = false;
  
  Particle (int x, int y, int z) {
    acceleration = new PVector(random(-0.2, 0.2), random(-0.2, 0.2), 0.1);
    velocity = new PVector(random(-0.2, 0.2), random(-0.2, 0.2), 5);
    position = new PVector(x, y, z);
  }

  public void run() {
    update();
    display();
  }

  private void update() {
    velocity.z = sin(5) / 30;
    velocity.add(acceleration);
    position.add(velocity);
    age++;
    
    h += up ? .3 : -.3;
    b -= 3;
    fill = color(h, 255, b);
  }

  private void display() {
    stroke(fill);
    fill(fill);
    point(position.x, position.y, position.z); 
  }
  
  public boolean done () {
    if (age > 80) {
      return true;
    }
    return false;
  }
}
